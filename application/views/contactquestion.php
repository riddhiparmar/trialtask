<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
    <title>
        Send Email via Mailgun API Using PHP
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="<?php echo site_url("js/jquery-1.11.3.min.js") ?>" type="text/javascript"></script>
    <script src="<?php echo site_url("js/bootstrap.js") ?>" type="text/javascript"></script>
    <link href="<?php echo site_url('css/bootstrap.css') ?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="<?php echo site_url('css/style.css') ?>" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container">
    <div class="row">
        <?php $gl = $this->session->flashdata('globalmsg'); ?>
        <?php if (!empty($gl)) : ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success"><b><span
                                    class="glyphicon glyphicon-ok"></span></b> <?php echo $this->session->flashdata('globalmsg') ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php $el = $this->session->flashdata('errormsg'); ?>
        <?php if (!empty($el)) : ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger"><b><span
                                    class="glyphicon glyphicon-remove"></span></b> <?php echo $this->session->flashdata('errormsg') ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="col-md-12">
            <div id="main">
                <h1>Trial Task of Contact Form With Mailgun and Airtable</h1>
            </div>
        </div>
        <div class="col-md-12">
            <div class="matter">
                <div id="login">
                    <h2>Get in touch with us as for your query</h2>
                    <hr/>
                    <?php echo form_open_multipart(site_url("contactquestion/send_mail"), array("class" => "form-horizontal", "id" => "contactform")) ?>
                    <label class="lab label">First Name</label>
                    <input required type="text" name="firstName" id="to" placeholder="First Name" pattern="[a-z]{1,15}"/><br/><br/>
                    <label class="lab">Last Name</label>
                    <input type="text" name="lastName" id="to" placeholder="Last Name"/><br/><br/>
                    <label class="lab label">Email Address</label>
                    <input required type="email" name="email_address" id="email_address"
                           placeholder="Email address" pattern="[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}"/><br/><br/>
                    <label class="lab label">Question</label>
                    <div class="clr"></div>
                    <textarea type="text" name="question" id="question" placeholder="Enter your question here.."
                              required></textarea><br><br>
                    <input type="submit" value=" Send " name="submit"/><br/>
                    <span></span>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Right side div -->
</div>
</body>
</html>
