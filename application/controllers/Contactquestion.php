<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';
use Mailgun\Mailgun;
use \TANIOS\Airtable\Airtable;

class Contactquestion extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('cookie');
        $this->load->library('session');
        $this->load->model('contact_model');

    }

    public function index()
    {
        $this->load->view('contactquestion');
    }

    public function send_mail()
    {
        $result = '';
        if (!empty($_POST['submit'])) {
            if (!empty($_POST['firstName']) && !empty($_POST['email_address'] && !empty($_POST['question']))) {
                # Instantiate the client.
                $mgClient = new Mailgun(MAILGUNAPIKEY);
                $domain = MAILGUNDOMAIN;

                $textForm = 'Query mail with below details : ';
                $textForm .= 'First Name : ' . $_POST['firstName'] . '<br/>';
                if (!empty($_POST['lastName'])) {
                    $textForm .= 'Last Name : ' . $_POST['lastName']. '<br/>';
                }
                $textForm .= 'Email Address' . $_POST['email_address']. '<br/>';
                $textForm .= 'Question' . $_POST['question'];

                # Make the call to the client.
                $result = $mgClient->sendMessage($domain, array(
                    'from' => 'Riddhi Webdimensions <postmaster@sandbox5b9920f5a6e14066832fc03a6bc3f2d6.mailgun.org>',
                    'to' => 'Vijay <vijay@csahealthbox.com>',
                    'cc' => 'Vijay <riddhiparmar3004@gmail.com>',
                    'subject' => 'Question contact form',
                    'text' => $textForm,
                    'html' => 'html'
                ));
                if ($result->http_response_code == 200) {
                    $this->create_data_airtable($_POST);
                } else {
                    $this->session->set_flashdata("errormsg", $result);
                    redirect('/');
                }
            } else {
                $this->session->set_flashdata("errormsg", "Astrick Mark Fields Are required");
                redirect('/');
            }
        } else {
            $this->session->set_flashdata("errormsg", "Astrick Mark Fields Are required");
            redirect('/');
        }
    }

    public function create_data_airtable($data)
    {
        $tableData = array(
            "first_name" => $data['firstName'],
            "last_name" => $data['lastName'],
            "email_address" => $data['email_address'],
            "question" => $data['question'],
            "created_at" => date("Y-m-d h:i:sa"),
            "updated_at" => date("Y-m-d h:i:sa"),
        );
        $this->contact_model->insertQuestionData($tableData);
        $airTableData = array(
            "first_name" => $data['firstName'],
            "last_name" => $data['lastName'],
            "email_address" => $data['email_address'],
            "question" => $data['question'],
        );
        $airtable = new Airtable(array(
            'api_key' => AIRTABLEAPIKEY,
            'base' => AIRTABLEBASE
        ));

        // Save to Airtable
        $new_contact = $airtable->saveContent(AIRTABLERECORDSTABLE, $airTableData);
        if(!empty($new_contact->id)){
            $this->session->set_flashdata("globalmsg", "Mail Forwarded with MailGUN, Data inserted in Database, Airtable");
            redirect('/');
        }else{
            $this->session->set_flashdata("errormsg", "Astrick Mark Fields Are required");
            redirect('/');
        }
    }

}
